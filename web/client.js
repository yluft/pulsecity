/*global PalmSystem io */
 
// Set up a socket.io channel with the backend server
var socket, // socket.io connection with server
    Commands, // Local commands the server can send us
    container,
    playerTypes; // The HTML container for all the Sprites

var sockets={};
 

// Number to color lookup table.
// The backend sends numbers, the css expects strings.

var colors = ['space', 'red', 'brown', 'purple', 'blue', 'orange', 
              'green', 'yellow'];

var types = ['Tile', 'Building', 'Populator'];

//var selectedSide;

var selected, // The currently selected piece (if any)
    tiles = {}, // Index of all pieces for easy reference
	coordinates = {},
	  zIndex = 1000,
	tilesNum = 0,
	played = false,
	sizeX, sizeY; // Used to make moving divs always on top.

var hadChosenType = false;
var selectTurn =0;


///////////////////////////////////////////////////////////////////////////////
//                  Visual functions                    //
///////////////////////////////////////////////////////////////////////////////


function ShowMessage(txt, interval) {
	console.log("ShowMessage : "+ txt);
	if(!interval) {
	  interval=5000;
	}
	$("#msgbox h1").text(txt);

	$("#msgbox").fadeIn('slow', function(){   setTimeout(function(){$("#msgbox").fadeOut('slow',function () { $("#msgbox .h1").text(""); })},interval);  })
}



///////////////////////////////////////////////////////////////////////////////
//                  Constructors and Prototypes (Classes)                    //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//                  User - save inforamtion about client                    //
///////////////////////////////////////////////////////////////////////////////
function User(){
  this.Name;
  this.type;
  this.ID;
}
var client = new User();
client.type = 'Tile';

///////////////////////////////////////////////////////////////////////////////
// 		Tile - shared parent for Piece, Space and suggestion         //
///////////////////////////////////////////////////////////////////////////////


function Tile(x, y, building, population) {
  var type;
  if (building == undefined || population == undefined) {
	type = 'space';
  } else if (building == false) {
	type = 'land'
  } else if (building == true && population == 0) {
	type = 'building';
  } else if (building == true && population > 0) {
	type = 'population'+population;
  }
  this.building = building;
  this.population = population;
  this.type = type;
  this.div = this.renderDiv(type);
  $(this.div).attr("data-x", x);
  $(this.div).attr("data-y", y);
  this.setTransform(x, y);
  // console.log("tilesNum " + tilesNum + ", num " + this.num);
  // tiles[tilesNum] = this;
  tilesNum++;
}

Tile.prototype.renderDiv = function (className) {
  if (this.div) {
	var div = this.div;
	var cb = function (el) {el.parentNode.removeChild(el)}
	$(div).fadeOut('fast', 'swing', cb(div));
  } else {
	var div = this.div = document.createElement('div');
  }
  div.setAttribute('class', className);
  div.sprite = this;
  container.appendChild(div);
  return div;
};

Tile.prototype.addBuilding = function () {
  this.building = true;
  this.renderDiv('building');
}

Tile.prototype.removeBuilding = function () {
  this.building = false;
  this.renderDiv('land');
}
 
Tile.prototype.addPop = function () {
  console.log("addPop on "+this.id);
  this.population++;
  this.renderDiv('population'+this.population);
}

Tile.prototype.removePop = function () {
  console.log("removePop on "+this.id);
  this.population--;
  if (this.population <= 0) {
	this.renderDiv('building');
	this.population = 0;
  } else {
	this.renderDiv('population'+this.population);
  }
}

// Move the sprite to a specified offset using hardware accel.
Tile.prototype.setTransform = function (x, y) {
  this.x = x;
  this.y = y;
  var px = x * (55 + 35),
      py =  (y * 90)-90 ;//- (x % 2) * 32

  this.div.style.webkitTransform = "translate(" + px + "px, " + py + "px)";
  this.div.style.MozTransform = "translate(" + px + "px, " + py + "px)";
  this.div.style['-ms-transform']  = "translate(" + px + "px, " + py + "px)";
  this.div.style['-moz-transform']  = "translate(" + px + "px, " + py + "px)";
};

Tile.prototype.destroy = function () {
  container.removeChild(this.div);
};

// Super minimal OOP inheritance library
Tile.adopt = function (child) {
  child.__proto__ = this;
  child.prototype.__proto__ = this.prototype;
  child.parent = this;
};

function select() {
  if (selected) {
	selected.deselect();
  }
  selected = this;
  console.log("selected "+this);
  $(this).addClass('select');
  // this.div.style.display = "block";
}

function deselect() {
  if (selected === this) {
    selected = null;
  }
  console.log("deselected "+this);
  $(this).removeClass('select');
  // this.div.style.display = "none";
}

function swapTiles(a, b) {
  if (a.type != 'space' && b.type == 'space') {
	console.log("PlayerSetAction: type: "+client.type+" z: "+ a.id+" x: "+b.x+" y: "+b.y);
	socket.send({PlayerSetAction: {type: client.type, action: 'Swap', z: a.id, x: b.x, y: b.y}});
	swapPlace(a, b);
	played = true;
	ShowMessage('Wait next round', 1000);
  } else {
	alert("Illegal move");
  }
}

function swapBuildings(a, b) {
  if (a.building && a.population == 0 && !b.building) {
	a.removeBuilding();
	b.addBuilding();
	played = true;
	console.log("PlayerSetAction: type: "+client.type+" z: "+ a.id+" x: "+b.x+" y: "+b.y);
	socket.send({PlayerSetAction: {type: client.type, action: 'Swap', z: a.id, x: b.x, y: b.y}});
	ShowMessage('Wait next round', 1000);
  } else {
	alert("illegal move");
  }
}

function movePeople(a, b) {
  if (a.population >= 0 && b.population < 3 && b.building) {
	a.removePop();
	b.addPop();
	played = true;
	console.log("PlayerSetAction: type: "+client.type+" z: "+ a.id+" x: "+b.x+" y: "+b.y);
	socket.send({PlayerSetAction: {type: client.type, action: 'Swap', z: a.id, x: b.x, y: b.y}});
	ShowMessage('Wait next round', 1000);
  } else {
	alert("illegal move");
  }
}

function swap(a, b, property) {
  console.log("SWAP " + property + " a " + a + " b " + b);
  played = true;
  if (property == 'tile') {
	swapTiles(a, b);
  } else if (property == 'building') {
	swapBuildings(a, b);
  } else if (property == 'settler') {
	movePeople(a, b);
  }
}

function canInteract(tile) {
  if (client.type == 'TileMover') {
	if (tile.type == 'land') {
	  return true;
	}
  } else if (client.type == 'BuildingMover') {
	if (tile.type == 'building') {
	  return true;
	}
  } else if (client.type == 'Settler') {
	if (tile.population > 0) {
	  return true;
	}
  }
  return false;
}

function swapPlace(a, b) {
  var ax = a.x, ay = a.y, bx = b.x, by = b.y;
  a.moveTo(bx, by, "2");
  b.moveTo(ax, ay, "2");
}

Tile.prototype.select = select;

Tile.prototype.deselect = deselect;

Tile.prototype.onClick = function (evt) {
  if (played) {
	return;
  }
  console.log('client.type='+client.type);
  console.log("this.type="+this.type);
  console.log("selected="+selected);
  if (selected == "happy") {
	socket.send({PlayerSetAction: {type: client.type, action: 'mood', z: "happy", x: this.x, y: this.y}});
	$(this.div).append("<img class='smiley' src='/art/Happy"+client.type+".png' style='position:relative; z-index:10000; left:0px; top:0px; width:25px; height:25px;'/>");
	played = true;
	ShowMessage('Wait next round', 1000);
	return;
  } 
  if (selected == "sad") {
	socket.send({PlayerSetAction: {type: client.type, action: 'mood', z: "sad", x: this.x, y: this.y}});
	$(this.div).append("<img class='smiley' src='/art/Sad"+client.type+".png' style='position:relative; z-index:10000; left:0px; top:0px; width:25px; height:25px;'/>");
	played = true;
	ShowMessage('Wait next round', 1000);
	return;
  } 
  if (selected === this) {
	console.log("deselect self");
	selected.deselect();
  } else if (selected) {
	console.log("got selected object");
	if (client.type == 'TileMover') {
	  if (this.type == 'space' && selected.type == 'land') {
		console.log("DEBUG: selected "+selected);
		swap(selected, this, 'tile');
	  }
	  selected.deselect();
	} else if (client.type == 'BuildingMover') {
	  swap(selected, this, 'building');
	  selected.deselect();
	} else if (client.type == 'Settler') {
	  swap(selected, this, 'settler');
	  selected.deselect();
	} else {
	}
  } else {
	this.select();
  }
  // this.renderDiv();
};

///////////////////////////////////////////////////////////////////////////////
// Space - grid spaces on the board
///////////////////////////////////////////////////////////////////////////////

function Space(x, y) {
  Tile.call(this, x, y, undefined, undefined);
}

Tile.adopt(Space);

Tile.prototype.moveTo = function (x, y, time) {
  time = time || 1;
  this.div.style.zIndex = zIndex++;
  this.div.style.webkitTransitionDuration = time + "s";
  this.setTransform(x, y);
};

///////////////////////////////////////////////////////////////////////////////
// Piece - a placed piece selected by team
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//                           External API Commands                           //
///////////////////////////////////////////////////////////////////////////////
Commands = {
  reload: function () {
    window.location.reload();
  },
  ChoosePlayerType: function(types){
	//console.log(types);
	// alert("ChoosePlayerType " + types);
	playerTypes = types;
	$('#chooseType').show();
	if (hadChosenType) {
	  var p = document.createElement("p");
	  p.innerHTML = "That type was already been chosen by another player, choose another:";
	  document.getElementById('chooseType').appendChild(p);
	}
	for (type in types) {
	  var e = document.createElement("span");
	  e.id = type;
	  // var child = document.createElement("p");
	  // child.innerHTML = type;
	  // child.onclick = function () {
	  e.innerHTML = type;
	  e.onclick = function () {
		socket.send({SelectPlayerType: this.innerHTML});
		console.log("sent type "+this.innerHTML);
		hadChosenType = true;
		$('#chooseType').hide();
		document.getElementById('chooseType').innerHTML = 'Waiting for other players';
	  };
	  // e.appendChild(child);
	  document.getElementById('chooseType').appendChild(e);
	}
	// $("#chooseType").show();
  },
  privateSolution: function (params) {
	console.log("privateSolution ");
	console.log(params);
	params.privateSolution;
	s = $(".righttoolbar-solution");
	for(var x=0 ; x < params.w; x++) {
		var row = $("<div/>");
		row.addClass("row"+x);
		row.addClass("row_solution");
		for(var y=0; y < params.h; y++) {
			div = $("<div/>");
			div.addClass( x + "_" + y + "_solution");
			div.addClass("solutions-items");
			a = 100/params.w+"%";
			div.html("&nbsp;");
//			div.width(a);
			row.append(div);
			
		}
		s.append(row);
	}
	pr = params.privateSolution;
	for(s in  pr) {
		$("."+(pr[s].x-1)+ "_"+(pr[s].y-1)+"_solution" ).addClass("selected");
		if(client.type=="Settler") {
		$("."+(pr[s].x-1)+ "_"+(pr[s].y-1)+"_solution" ).text(pr[s].population);
		}
	}
	
  },
  ConfirmPlayerType: function(type) {
	console.log("ConfirmPlayerType");
	client.type = type;
	displayPlayerInfo();
  },
  ShowTutorial: function(interval) {
	console.log("got ShowTurorial");
	setTimeout(function() {$("#tutorial").hide()}, interval);
	$("#tutorial").show();
  },
  Pulse: function(mockup) {
	console.log("GOT PULSE");
	PlayHartbeat();
  },
  StartRound: function (params) {
	tilesNum = 0;
	console.log("got StartRound " + params);
	sizeX = params.SizeX;
	sizeY = params.SizeY;
	drawBoard(params.presets);
  },
  clientDisconected: function() {
	console.log("You've been fucked by the client!");
  },
  StartTurn: function (board) {
	tilesNum = 0;
	selected = null;
	played = false;
	console.log("StartTurn");
	var keys = [];
	for(var k in board) keys.push(k);
	console.log("StartRound handler presets: "+keys);
	drawBoard(board);
  },
  
};
function PlayHartbeat() {
    var sound = document.getElementById("heartbeat");
   console.log(sound);
    sound.cloneNode(true).play();
}
function displayPlayerInfo() {

}

function drawBoard(board) {
  $(".smiley").remove();
  for (tile in board) {
	var lx = board[tile].x, ly = board[tile].y; 
	console.log("tile:"+tile+" new : x: "+lx+" y: "+ ly + " b: " + board[tile].building+" p: "+board[tile].population);
	if (!tiles[tile]) {
	  tiles[tile] = new Tile(lx, ly, board[tile].building, board[tile].population);
	  tiles[tile].id = tile;
	  coordinates[lx+','+ly] = tiles[tile];
	} else {
	  if (tiles[tile].x != board[tile].x || tiles[tile].y != board[tile].y) {
		tiles[tile].moveTo(board[tile].x, board[tile].y, '2');
	  }
	  if (tiles[tile].building == false && board[tile].building == true) {
		tiles[tile].addBuilding();
	  } else if (tiles[tile].building == true && board[tile].building == false) {
		tiles[tile].removeBuilding();
	  }
	  if (tiles[tile].population < board[tile].population) {
		tiles[tile].addPop();
	  } else if (tiles[tile].population > board[tile].population) {
		tiles[tile].removePop();
	  }
	}
	if (board[tile].mood) {
	  drawSmiley(tiles[tile].div,
		  board[tile].mood.player, board[tile].mood.mood);
	}
  }
  // console.log("sizeX: "+sizeX);
  // console.log("sizeY: "+sizeY);
  for (var x=1; x <= sizeX; x++) {
	for (var y=1; y <= sizeY; y++) {
	  // console.log("(x,y)="+x+","+y);
	  if (!coordinates[x+','+y]) {
		coordinates[x+','+y] = new Space(x, y);
	  }
	}
  }
}

function drawSmiley(div,player, mood) {
  console.log("drawSmiley " + player + " " + mood);
  if (mood == "happy") {
	$(div).append("<img class='smiley' src='/art/Happy"+player+".png' style='position:relative; z-index:10000; left:0px; top:0px; width:25px; height:25px;'/>");
  } else if (mood == "sad") {
	$(div).append("<img class='smiley' src='/art/Sad"+player+".png' style='position:relative; z-index:10000; left:0px; top:0px; width:25px; height:25px;'/>");
  }
  // TODO 
}

///////////////////////////////////////////////////////////////////////////////
//                           Initialization of window                        //
///////////////////////////////////////////////////////////////////////////////
window.addEventListener('load', function () {

  // Connect to the backend server for duplex communication
  if (window.location.protocol === 'file:') {
    socket = new io.Socket("localhost", {
      port: 8080,
      transports: ['xhr-polling']
    });
  } else {
     socket = new io.Socket();
  }
  var flail = true;
  function tryConnect() {
    if (flail) {
      socket.connect();
    }
  }
  setTimeout(tryConnect);
  setInterval(tryConnect, 10000);
  socket.on('connect', function () {
    flail = false;
  });
  socket.on('disconnect', function () {
    console.error("Got disconnected from server!");
    flail = true;
  });

  // Add functions to smilies
  $(".happy").click(function() {
	console.log("clicked happy");
	selected = "happy";
  });
  $(".sad").click(function() {
	console.log("clicked sad");
	selected = "sad";
  });

  // Forward messages from the backend to the Commands object in the client
  socket.on('message', function (message) {
    Object.keys(message).forEach(function (command) {
      if (Commands.hasOwnProperty(command)) {
        Commands[command](message[command]);
      } else {
        console.error("Invalid command " + command);
      }
    });
  });

  // Store a reference to the container div in the dom
  container = document.getElementById('sprites');

  // Always fit the sprite container to the window and even auto-rotate
  var width = container.clientWidth,
      height = container.clientHeight;

  function onResize() {
    var winWidth = window.innerWidth,
        winHeight = window.innerHeight;
    var vertical = (height > width) === (winHeight > winWidth);
    var transform;
    // removed vertical tansform
    //if (vertical) {
     // transform = "scale(" +    Math.min(winWidth / width, winHeight / height) + ")";
    //} else {
     // transform = "scale(" +Math.min(winWidth / height, winHeight / width) + ") rotate(-90deg)";
    //}
   // container.style.webkitTransform = transform;
  }

  window.addEventListener('resize', onResize);
  onResize();

  // Hook up mouse(down, move, up) and touch(down, move, up) to sprites
  function findSprite(target) {
    if (target === container) {
      return;
    }
    if (target.sprite) {
      return target.sprite;
    }
    if (!target.parentNode) {
      return;
    }
    return findSprite(target.parentNode);
  }
  var start;
  // Listen for mouse and touch events
  function onDown(evt) {
    start = findSprite(evt.target);
	console.log(start)
    if (!start) {
      return;
    }
    evt.stopPropagation();
    evt.preventDefault();
    if (start.onDown) {
      start.onDown(evt);
    }
  }
  function onMove(evt) {
    if (!start) {
      return;
    }
    evt.stopPropagation();
    evt.preventDefault();
    if (start.onMove) {
      start.onMove(evt);
    }
  }
  function onUp(evt) {
    if (!start) {
      return;
    }
    evt.stopPropagation();
    evt.preventDefault();
    if (start.onUp) {
      start.onUp(evt);
    } else if (start.onClick) {
      var end = findSprite(evt.target);
      if (start === end) {
        start.onClick(evt);
      }
    }
    start = undefined;
  }
  container.addEventListener('mousedown', onDown, false);
  document.addEventListener('mousemove', onMove, false);
  document.addEventListener('mouseup', onUp, false);


  container.addEventListener('touchstart', function (e) {
    if (e.touches.length === 1) {
      var touch = e.touches[0];
      touch.stopPropagation = function () {
        e.stopPropagation();
      };
      touch.preventDefault = function () {
        e.preventDefault();
      };
      onDown(touch);
    }
  }, false);
  document.addEventListener('touchmove', function (e) {
    if (e.touches.length === 1) {
      var touch = e.touches[0];
      touch.stopPropagation = function () {
        e.stopPropagation();
      };
      touch.preventDefault = function () {
        e.preventDefault();
      };
      onMove(touch);
    }
  }, false);
  document.addEventListener('touchend', onUp, false);

}, false);


function hideAddressBar()
{
    if(!window.location.hash)
    {
        if(document.height <= window.outerHeight + 10)
        {
            //document.body.style.height = (window.outerHeight + 50) +'px';
            setTimeout( function(){ window.scrollTo(0, 1); }, 50 );
        }
        else
        {
            setTimeout( function(){ window.scrollTo(0, 1); }, 0 );
        }
    }
} 
 
window.addEventListener("load", hideAddressBar );
window.addEventListener("orientationchange", hideAddressBar );

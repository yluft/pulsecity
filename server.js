var Connect = require('connect'),
    io = require('socket.io');

/*io.configure(function () { 
  io.set("transports", ["xhr-polling"]); 
  io.set("polling duration", 10); 
});*/

var clone = function() {
  var newObj = (this instanceof Array) ? [] : {};
  for (var i in this) {
    if (this[i] && typeof this[i] == "object") {
      newObj[i] = this[i].clone();
    } else newObj[i] = this[i]
  } return newObj;
}; 

Object.defineProperty( Object.prototype, "clone", {value: clone, enumerable: false});

// Allow the user to specify the port via environment variables
var PORT = process.env.PORT || 80;

// Serve static files in the web folder
var server = Connect.createServer(
  Connect.staticProvider(__dirname + '/web')
);


// Listen to socket.io traffic too
var socket = io.listen(server);
var gameStats = {PandingUsers : 1, InGame : 2, GameEnded : 3};

var game = {status:gameStats.PandingUsers};


// player count
var players = new Array(4);
var playerCount = 0;

//  game board size
var sizeX=8;
var sizeY=8; 
var presetCounter=0;


var clients = [];
var Players = [];
var PlayerUnusedTypes;
var PlayerTypes = 
{
	"TileMover": 1,
	"BuildingMover": 2,
//	"FloorAllocator": 3,
	"Settler": 4
}
PlayerUnusedTypes = PlayerTypes.clone();
var moods = { happy:1, unhappy:2};
function tile()
{
	this.building=false;
	this.floors=0;
	this.population=0;
}
function tile(b,p)
{	
	this.building=b;
	this.population=p;
}

var currentPreset;
var presets = 
{
	1 : {
		1: {x: 3,y:4,building:true,population:1},
		2: {x: 3,y:5,building:true,population:1},

		3: {x: 4,y:3,building:true,population:1},
		4: {x: 4,y:4,building:false,population:0},
		5: {x: 4,y:5,building:false,population:0},
		6: {x: 4,y:6,building:true,population:1},

		7: {x: 5,y:3,building:true,population:1},
		8: {x: 5,y:4,building:false,population:0},
		9: {x: 5,y:5,building:false,population:0},
		10: {x: 5,y:6,building:true,population:1},

		11: {x: 6,y:4,building:true,population:1},
		12: {x: 6,y:5,building:true,population:1}
	},
	2 : {
		1: {x: 3,y:4,building:true,population:1},
		2: {x: 3,y:5,building:true,population:1},

		3: {x: 4,y:3,building:true,population:1},
		4: {x: 4,y:4,building:true,population:0},
		5: {x: 4,y:5,building:false,population:0},
		6: {x: 4,y:6,building:true,population:1},

		7: {x: 5,y:3,building:true,population:1},
		8: {x: 5,y:4,building:false,population:0},
		9: {x: 5,y:5,building:true,population:0},
		10: {x: 5,y:6,building:true,population:1},

		11: {x: 6,y:4,building:true,population:1},
		12: {x: 6,y:5,building:true,population:1}

	}
};


solutionsMax = {
	1: {w:6,h:6},
	2: {w:5,h:4},

}

var solutions = 
{
	1 : {
		1: {x: 3,y:1,building:true,population:1},
		2: {x: 3,y:2,building:true,population:0},
		3: {x: 3,y:3,building:true,population:2},

		4: {x: 1,y:4,building:true,population:1},
		5: {x: 2,y:4,building:true,population:0},
		6: {x: 3,y:4,building:false,population:0},

		7: {x: 4,y:3,building:false,population:0},
		8: {x: 4,y:4,building:true,population:2},
		9: {x: 4,y:5,building:true,population:0},
		10: {x: 4,y:6,building:true,population:1},

		11: {x: 5,y:3,building:true,population:0},
		12: {x: 6,y:3,building:true,population:1}
	},
	2 : {
		1: {x: 1,y:1,building:true,population:2},
		2: {x: 2,y:1,building:true,population:0},
		3: {x: 3,y:1,building:true,population:0},
		4: {x: 4,y:1,building:true,population:0},
		5: {x: 5,y:1,building:true,population:2},

		6: {x: 4,y:2,building:false,population:0},
		7: {x: 4,y:3,building:false,population:0},

		8: {x: 1,y:4,building:true,population:2},
		9: {x: 2,y:4,building:true,population:0},
		10: {x: 3,y:4,building:true,population:0},
		11: {x: 4,y:4,building:true,population:0},
		12: {x: 5,y:4,building:true,population:2}
	}
};
var actions={};
var pulseTimeout;
var currentPulse=1;
var pulses = {
1: {timeout:30000},
2: {timeout:20000},
3: {timeout:10000},
4: {timeout:5000},
5: {timeout:5000},
6: {timeout:500}
}





function StartGame() 
{
	socket.broadcast({ShowTutorial:8000});
	setTimeout(StartRound,10000);
}
function StartRound()
{
	board = new Array(sizeX);
	presetCounter++;
	
	for (var x=0;x<sizeX;x++) 
	{
	    board[x]= new Array(sizeY);
	}

	x = presetCounter;
	currentPreset = presets[x].clone();
	for(y in presets[x]) {
		board[presets[x][y].x][presets[x][y].y] = new tile(presets[x][y].building,presets[x][y].population);
	}
	BrodcastSolutions();
	socket.broadcast( { StartRound:{SizeX: sizeX, SizeY: sizeY, presets : presets[x] } } );
	
	StartTurn();
}
function StartTurn() 
{
	actions={};
	currentPulse=0;
	PlayerUnusedTypes = PlayerTypes.clone();
	pulseTimeout = setTimeout(SendPulse, (10000*((currentPulse%2)+1)));
	socket.broadcast( { StartTurn:currentPreset } );
	// clear moods !!!
}
function SendPulse() 
{
	currentPulse++;
	if(pulses[currentPulse] == undefined) {
		socket.broadcast( { GameFaild:true } );		
		return;
	} else {
		socket.broadcast( { Pulse:true } );
		//pulseTimeout = setTimeout(SendPulse,10000);
		pulseTimeout = setTimeout(SendPulse,30000);
	}
}
function BrodcastSolutions() 
{
	var x = presetCounter;
	for (p in Players) {
		privateSolution={};
		if(p=="Settler") {
			for(s in solutions[x]) {
				if(solutions[x][s].population>0) {
					privateSolution[s] = solutions[x][s].clone();
				}
			}
		} else if (p=="TileMover") {
			for(s in solutions[x]) {
					privateSolution[s] = solutions[x][s].clone();
			}
		} else if(p=="BuildingMover") {
			for(s in solutions[x]) {
				if(solutions[x][s].building==true) {
					privateSolution[s] = solutions[x][s].clone();
				}
			}

		}
		Players[p].send( { privateSolution:{privateSolution : privateSolution, w:solutionsMax[x].w, h:solutionsMax[x].h} } );
	}
}

function moveTileAction(tile, newX, newY) {
	console.log("moveTileAction: oldX: "+currentPreset[tile].x
	+" oldY: "+currentPreset[tile].y
	+" newX: "+newX+" newY: "+newY);
	currentPreset[tile].x = newX;
	currentPreset[tile].y = newY;
}

function moveBuilding(origin, target) {
	console.log("moveBuilding: origin: "+origin +" target: "+target);
	if (!target || !origin) return;
	if (currentPreset[origin].building && !target.building) {
		currentPreset[origin].building = false;
		target.building = true;
	}
}

function movePopulation(origin, target) {
	console.log("movePopulation: origin: "+origin +" target: "+target);
	console.log("movePopulation: origin: "+origin +" target.pop: "+target.population);
	if (!target || !origin) return;
	if (currentPreset[origin].population > 0 
			&& target.population < 3
			&& target.building) {
		currentPreset[origin].population--;
		target.population++;
	}
	console.log("movePopulation: origin: "+origin +" target.pop: "+target.population);
}

function updateBoard() {
	var newBoard = new Array(board.length);
	for (var c = 0; c < newBoard.length; c++) {
		newBoard[c] = new Array(board[0].length);
		for (var r = 0; r < newBoard[c].length; r++) {
			newBoard[c][r] = undefined;
		}
	}
	for (tile in currentPreset) {
		var curTile = currentPreset[tile];
		console.log("updateBoard : curTile.x: "+curTile.x + " curTile.y: " + curTile.y);
		newBoard[curTile.x][curTile.y] = curTile;
		if (curTile.mood) {
			delete curTile.mood;
		}
	}
	for (var c = 0; c < newBoard.length; c++) {
		for (var r = 0; r < newBoard[c].length; r++) {
			board[c][r] = newBoard[c][r];
		}
	}
	for(k in PlayerTypes) {
		if (actions[k] != undefined && actions[k].action == 'mood') {
			var mood = {player: k, mood: actions[k].z};
			if (board[actions[k].x][actions[k].y]) {
				board[actions[k].x][actions[k].y].mood = mood;
			}
		}
	}
}

function CheckGame () 
{
	/*  after checing turn  */	
	for(k in PlayerTypes) {
		if(actions[k]!=undefined) {
			if(actions[k].action=="Swap") {
				board[actions[k].x][actions[k].y];
				if (k == 'TileMover') {
					moveTileAction(actions[k].z, actions[k].x, actions[k].y);
				} else if (k == 'BuildingMover') {
					moveBuilding(actions[k].z, board[actions[k].x][actions[k].y]);
				} else if (k == 'Settler') {
					movePopulation(actions[k].z, board[actions[k].x][actions[k].y]);
				} else {
					console.log(k+" is not player type");
				}
			} else {
			}
		}
	}
	updateBoard();
	isDone = CheckWinning();
	if(isDone==true) {
		socket.broadcast( { GameWon:true } );
		StartRound();
	}
	//	start another turn
	StartTurn();
}

function CheckWinning() 
{
	var s = presetCounter;
	solutions[s];
	var isDone=true;
	console.log("CheckWinning: s: "+s);
	for (x=0; x < board.length-(solutionsMax[s].w-1); x++) 
	{
		for( y=0; y < board[x].length-(solutionsMax[s].h-1); y++) {
		   isDone=true;
		    for(z in solutions[s]) {
			if(board[solutions[s][z].x+x]!=undefined) {
				if(board[solutions[s][z].x+x][solutions[s][z].y+y] ==  undefined 
					|| board[solutions[s][z].x+x][solutions[s][z].y+y].building !=  solutions[s][z].building 
					|| board[solutions[s][z].x+x][solutions[s][z].y+y].population !=  solutions[s][z].population) {
					isDone=false;
					continue;
				}
			} else {
				isDone=false;
				continue;
			}
		    }
		    if(isDone==true) {
			return isDone;
		    }

		}
	}

	return false;
	
}


// Every time a new client connects or reconnects, we get this
socket.on('connection', function (client) 
{
  
  // Send the client the initial map
  //client.send({GameState: game});
  client.send({ChoosePlayerType: PlayerUnusedTypes});
  // Define the commands we're willing to accept from the client
  var Commands = {
    SelectPlayerType: function(type) {
	if(game.status == gameStats.InGame) {
		console.log("Error - cannot add user in the middle of a game");
		return;
	}
	if(PlayerUnusedTypes[type]!=undefined) {
		delete PlayerUnusedTypes[type];
		client.send({ConfirmPlayerType: type});
		Players[type] = client;
	} else {
	  client.send({ChoosePlayerType: PlayerUnusedTypes});
	}

	if(Object.keys(PlayerUnusedTypes).length==0) {
		game.status = gameStats.InGame;
		StartGame();
	}
	
    },
    PlayerSetAction: function(params) {
		//type,action, z,x,y
	console.log("PlayerSetAction");
	console.log(params.type+","+params.action+", "+params.z+", "+params.x+", "+params.y);
	if(PlayerUnusedTypes[params.type]!=undefined) {
		delete PlayerUnusedTypes[params.type];
		actions[params.type] = {action: params.action,z:params.z,x:params.x,y:params.y};
	}
//	console.log("PlayerUnusedTypes : " + Object.keys(PlayerUnusedTypes).length);
	if(Object.keys(PlayerUnusedTypes).length==0) {
		//console.log("CheckGame");
		CheckGame();
	}
    }
  };

  Commands.__proto__ = client;
  clients.push(Commands);

  client.on('disconnect', function () {
    clients.splice(clients.indexOf(Commands), 1);
    socket.broadcast({clientDisconected:Commands});

  });

  // Route messages to the Commands object
  client.on('message', function (message) {
    Object.keys(message).forEach(function (command) {
      if (Commands.hasOwnProperty(command)) {
        Commands[command](message[command]);
      } else {
        console.error("Invalid command " + command);
      }
    });
  });
});

server.listen(PORT);
console.log("Server running at port http://localhost:%s/", PORT);

